# emojofy
Easily make your text a e s t h e t i c or otherwise :b: etter

# installation

`cargo install emojofy`

# usage

```
emojofy 0.2.0


USAGE:
    emojofy [OPTIONS] <words>...

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -a, --after <after>                The string to put after each character, with an underscore in between it and the
                                       character
    -b, --before <before>              The string to put before each character, with an underscore in between it and the
                                       character
    -p, --prefix <prefix>              The string to literally put before each character
    -s, --suffix <suffix>              The string to literally put after each character
    -w, --word-separator <word_sep>    The string to separate each word with [default: 
                                       ]

ARGS:
    <words>...    

EXAMPLES:
$ emojofy -w '   ' aesthetic words
a e s t h e t i c   w o r d s

$ emojofy -b hacker hackerman
:hacker_h: :hacker_a: :hacker_c: :hacker_k: :hacker_e: :hacker_r: :hacker_m: :hacker_a: :hacker_n:

$ emojofy -s wide squernch
:swide: :qwide: :uwide: :ewide: :rwide: :nwide: :cwide: :hwide:
```

# TODO

* Add some way of excluding certain words or characters to make pipelines with `tr` and `printf` better, so you can mix styles.
* Add some examples of said pipelines
* Intelligently add a newline to the end if output is to a terminal (although maybe this should be handled by the shell)
* awoo