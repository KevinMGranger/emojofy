#[macro_use]
extern crate clap;

use clap::{App, Arg};

fn arg_parser() -> App<'static, 'static> {
    let prepend = Arg::from_usage(
        "-p --prefix <prefix> 'The string to literally put before each character'",
    ).conflicts_with("before")
        .required(false);
    let before = Arg::from_usage("-b --before <before> 'The string to put before each character, with an underscore in between it and the character'").conflicts_with("prefix").required(false);
    let suffix = Arg::from_usage(
        "-s --suffix <suffix> 'The string to literally put after each character'",
    ).conflicts_with("after")
        .required(false);
    let after = Arg::from_usage("-a --after <after> 'The string to put after each character, with an underscore in between it and the character'").conflicts_with("suffix").required(false);
    let word_sep = Arg::from_usage(
        "[word_sep] -w --word-separator <word_sep> 'The string to separate each word with'",
    ).default_value("\n");

    let words = Arg::with_name("words").required(true).multiple(true);

    App::new(crate_name!())
        .version(crate_version!())
        .about(crate_description!())
        .args(&[prepend, before, suffix, after, word_sep, words])
        .after_help(
            "EXAMPLES:
$ emojofy -w '   ' aesthetic words
a e s t h e t i c   w o r d s

$ emojofy -b hacker hackerman
:hacker_h: :hacker_a: :hacker_c: :hacker_k: :hacker_e: :hacker_r: :hacker_m: :hacker_a: :hacker_n:

$ emojofy -s wide squernch
:swide: :qwide: :uwide: :ewide: :rwide: :nwide: :cwide: :hwide:",
        )
}

fn main() {
    let matches = arg_parser().get_matches();

    let pre = if let Some(prefix) = matches.value_of("prefix") {
        prefix.to_string()
    } else if let Some(before) = matches.value_of("before") {
        format!("{}_", before)
    } else {
        "".to_string()
    };

    let post = if let Some(suffix) = matches.value_of("suffix") {
        suffix.to_string()
    } else if let Some(after) = matches.value_of("after") {
        format!("_{}", after)
    } else {
        "".to_string()
    };

    let maybe_colon = if pre.len() != 0 || post.len() != 0 {
        ":"
    } else {
        ""
    };

    let word_sep = matches.value_of("word_sep").unwrap();

    let words = matches.values_of("words").unwrap();

    for word in words {
        let mut chars = word.chars();
        if let Some(chr) = chars.next() {
            print!("{}{}{}{}{}", maybe_colon, pre, chr, post, maybe_colon);
            for chr in chars {
                print!(" {}{}{}{}{}", maybe_colon, pre, chr, post, maybe_colon);
            }
        }
        print!("{}", word_sep);
    }
}
